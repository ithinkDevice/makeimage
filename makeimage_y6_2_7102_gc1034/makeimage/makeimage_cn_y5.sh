#!/bin/sh
echo version=$1 > /home/rootfs_uClibc_SDK2/rootfs_uClibc/conf/version
if [ ! -f "jffs2.img" ]; then
	echo "make jffs2.img..."
	##################add##############################################################
	cp /disk/yaoqiu/ithinkAPP/y6_2/y6_2_$2_ithink/*   /gk7102c/jffs2jxh42cnc1_2/app/itk_ipc   -rf
	#################sdd end############################################################
	mkfs.jffs2 -p -l -e 0x10000 -r /gk7102c/jffs2jxh42cnc1_2/  -o jffs2.img	
else
	rm jffs2.img -rf
	echo "make jffs2.img..."   
	##################add##############################################################
	cp /disk/yaoqiu/ithinkAPP/y6_2/y6_2_$2_ithink/*   /gk7102c/jffs2jxh42cnc1_2/app/itk_ipc   -rf
	#################sdd end############################################################
	mkfs.jffs2 -p -l -e 0x10000 -r /gk7102c/jffs2jxh42cnc1_2/  -o jffs2.img	
fi
sleep 1
mv jffs2.img  /gk7102c/makeimage_v2.1_sj/APP/jffs2.img -f
echo "make jffs2.img ok!"

if [ ! -f "squashfs.img" ]; then
	echo "make squashfs.img..."
	###############################add#####################################
	rm -rf /home/rootfs_uClibc_SDK2/rootfs_uClibc/usr/aac/*
	cp /disk/AACfiaml/aac16000_ithink/*  /home/rootfs_uClibc_SDK2/rootfs_uClibc/usr/aac/   -rf 
	############################add end####################################
mksquashfs /home/rootfs_uClibc_SDK2/rootfs_uClibc squashfs.img -b 128K
else
	rm squashfs.img -rf
	echo "make squashfs.img..."
	###############################add#####################################
	rm -rf /home/rootfs_uClibc_SDK2/rootfs_uClibc/usr/aac/*
	cp /disk/AACfiaml/aac16000_ithink/*  /home/rootfs_uClibc_SDK2/rootfs_uClibc/usr/aac/   -rf 
	############################add end####################################

mksquashfs /home/rootfs_uClibc_SDK2/rootfs_uClibc squashfs.img -b 128K
fi
mv squashfs.img /gk7102c/makeimage_v2.1_sj/filesystem/squashfs.img -f
echo "make squashfs.img ok!"
###################################################################################
sed -i '/version=/d'   /gk7102c/makeimage_v2.1_sj/uboot_env_gk7102.txt
echo version=$1 >>     /gk7102c/makeimage_v2.1_sj/uboot_env_gk7102.txt
cd /gk7102c/makeimage_v2.1_sj/
rm  ./build/* -r
make -f Makefile
sed  -i '\/APP\/jffs2.img/a\version='"$1"       ./build/gk7102-ITHINK_image_sd_update.cfg #########add
echo update_zone=11100 >>                      ./build/gk7102-ITHINK_image_sd_update.cfg
mv ./build/gk7102-ITHINK_image_sd_update.cfg   ./build/gk710x_image_sd_update.cfg -f  
mv ./build/gk7102-ITHINK_image.bin             ./build/gk710x_image_sd_update.bin -f                                                   
################################################################################
mkdir ./build/gk7102_cn_y6_2_$2_sd_$1 
mkdir ./build/gk7102_cn_y6_2_$2_sd_$1/flash
mv    ./build/gk710x_image_sd_update.cfg         ./build/gk7102_cn_y6_2_$2_sd_$1
mv    ./build/gk710x_image_sd_update.bin         ./build/gk7102_cn_y6_2_$2_sd_$1
mv    ./build/gk7102-ITHINK_image_swap.bin       ./build/gk7102_cn_y6_2_$2_sd_$1/flash
##################################################################
cp    ./filesystem         ./build/gk7102_cn_y6_2_$2_sd_$1 -rf
cp    ./kernel             ./build/gk7102_cn_y6_2_$2_sd_$1 -rf
##################################################################
tar   zcvf  gk7102_cn_y6_2_$2_sd_$1.tar.gz ./build/gk7102_cn_y6_2_$2_sd_$1
