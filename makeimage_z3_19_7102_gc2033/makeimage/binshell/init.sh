#!/bin/sh

kernel_ver=$(uname -r)

insmod /lib/modules/$kernel_ver/extra/dsp.ko
insmod /lib/modules/$kernel_ver/extra/vo.ko
insmod /lib/modules/$kernel_ver/extra/vi.ko
insmod /lib/modules/$kernel_ver/extra/isp.ko
insmod /lib/modules/$kernel_ver/extra/hw_crypto.ko
insmod /lib/modules/$kernel_ver/extra/media.ko
insmod /lib/modules/$kernel_ver/extra/audio.ko
#insmod /lib/modules/$kernel_ver/extra/ptz_drv.ko
case "$?" in
    0)
	;;
    *)
  	echo "insert media module failed!"
	exit 1
	;;
esac

insmod /lib/modules/$kernel_ver/extra/tv.ko
case "$?" in
    0)
	;;
    *)
  	echo "insert cvbs module failed!"
	exit 1
	;;
esac

default_sensor=jxh42
case $1 in
    --imx222 ) insmod /lib/modules/$kernel_ver/extra/imx222.ko ;;
    --ov2710 ) insmod /lib/modules/$kernel_ver/extra/ov2710.ko ;;
    --ov9710 ) insmod /lib/modules/$kernel_ver/extra/ov9710.ko ;;
    --ov9715 ) insmod /lib/modules/$kernel_ver/extra/ov9710.ko ;;
    --ar0130 ) insmod /lib/modules/$kernel_ver/extra/ar0130.ko ;;
    --ar0330 ) insmod /lib/modules/$kernel_ver/extra/ar0330.ko ;;
    --jxh42  ) insmod /lib/modules/$kernel_ver/extra/jxh42.ko  ;;
    --jxh61  ) insmod /lib/modules/$kernel_ver/extra/jxh61.ko  ;;
    --bg0701 ) insmod /lib/modules/$kernel_ver/extra/bg0701.ko ;;
    --sc1035 ) insmod /lib/modules/$kernel_ver/extra/sc1035.ko ;;
    --sc1135 ) insmod /lib/modules/$kernel_ver/extra/sc1135.ko ;;
    --sc1045 ) insmod /lib/modules/$kernel_ver/extra/sc1045.ko ;;
    --gc1024 ) insmod /lib/modules/$kernel_ver/extra/gc1024.ko ;;
    --mis1002) insmod /lib/modules/$kernel_ver/extra/mis1002.ko ;;
    --sc2035 ) insmod /lib/modules/$kernel_ver/extra/sc2035.ko ;;
    --sc1145 ) insmod /lib/modules/$kernel_ver/extra/sc1145.ko ;;
	--imx323 ) insmod /lib/modules/$kernel_ver/extra/imx323.ko ;;
    --na )     echo "Init without sensor..." ;;
    *)         echo "Default method to init without VI drivers..."
               insmod /lib/modules/$kernel_ver/extra/$default_sensor.ko ;;
esac
case "$?" in
    0)
	;;
    *)
  	echo "Insmod $1 failed!"
	exit 1
	;;
esac


exit $?

